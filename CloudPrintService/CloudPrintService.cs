﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using NLog;
using CloudPrint;
using CloudPrint.Config;
using System.Configuration;

namespace CloudPrintService
{
    public partial class CloudPrintService : ServiceBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private int eventId = 1;
        private List<CloudPrintConfig> cloudPrintConfigs;
        private DocumentManager dm;

        public CloudPrintService()
        {
            InitializeComponent();

            dm = new DocumentManager();
            cloudPrintConfigs = CloudPrintConfigManager.LoadConfigsFromFile();

            if (cloudPrintConfigs.Count == 0)
            {
                logger.Error("No configurations found.");

                throw new Exception("No configurations found. Exiting.");
            }

            logger.Trace("Loaded {0} configurations from file.", cloudPrintConfigs.Count.ToString());
            logger.Info("Cloud printService initialized");
        }

        protected override void OnStart(string[] args)
        {
            string secondsString = ConfigurationManager.AppSettings["ServiceRunIntervalSeconds"];

            if (secondsString == string.Empty
                || !Int32.TryParse(secondsString, out int secondsInt))
            {
                logger.Error("Missing or incorrect value for parameter ServiceRunIntervalSeconds in app configuration");

                throw new InvalidProgramException("Missing or incorrect value for parameter ServiceRunIntervalSeconds in app configuration");
            }


            // Set up a timer 
            System.Timers.Timer timer = new System.Timers.Timer
            {
                Interval = secondsInt * 1000 // transform to milliseconds
            };
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();

            logger.Info("Cloud printService started with interval {0} seconds.", secondsInt);
        }

        protected override void OnStop()
        {
            logger.Info("Cloud printService stopped");
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            if (eventId > 1000000)
            {
                eventId = 0;
            }

            logger.Trace("Scanning for new documents to print. Current execution count {0} (restored to 0 every 1 000 000 times).", eventId++);

            dm.PrintAllDocuments(cloudPrintConfigs);

            if (eventId % 100 == 0) // Check to remove files every 100 times
            {
                dm.RemoveDownloadedFiles(cloudPrintConfigs);
            }
        }

        internal void TestStartupAndStop(string[] args)
        {
            dm.PrintAllDocuments(cloudPrintConfigs);
            Console.ReadLine();
        }
    }
}
