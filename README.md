# Implema Cloud Print
This application runs as a Windows Service and prints pdf documents from a Azure Blob Storage and prints them on a local or network printer.

## Getting started
Clone and build this repository.

## Install
Install through the compiled MSI-file
Note that the installer is currently only performing the necessary changes in registry as well as installing the CloudPrintService.exe. To make the installation complete, the following files must be copied to the install dir (the WIX installer should be completed with these files)

- CloudPrint.dll
- CloudPrintService.exe.config
- Microsoft.Azure.KeyVault.Core.dll
- Microsoft.Azure.Storage.Blob.dll
- Microsoft.Azure.Storage.Common.dll
- Newtonsoft.Json.dll
- NLog.config
- NLog.dll
- RawPrint.dll

## Configuration
Two files needs configuration

### CloudPrintService.exe.config

One must configure connection string to an Azure storage account and poll frequency (seconds). Also, one must configure print configurations. 

Please configure the below sections

` <appSettings>
    <add key="ServiceRunIntervalSeconds" value="10" /> <!-- Seconds between Azure Storage polling-->
  </appSettings>`

`<connectionStrings>
    <clear />
    <add name="BlobStorage" 
            connectionString="DefaultEndpointsProtocol=https;AccountName=[account name];AccountKey=[account key];EndpointSuffix=core.windows.net" />
  </connectionStrings>`

  `<configSections>
    <section name="CloudPrintConfigSection" 
                type="CloudPrint.Config.CloudPrintConfigurationSection, CloudPrint" />
  </configSections>`
  
  `<CloudPrintConfigSection>
    <Configurations>
      <add FolderName="[blob storage container]" 
            PrinterName="[printer name]" 
            ArchiveFolderName="[Archive container name]" 
            FailedFolderName="[Failed container name]" 
            DownloadBeforePrint="[true | false]" 
            PrintWith="[RawPrint | Adobe | FoxIt]" 
            FileDownloadPath="[local file path]" />
    </Configurations>
  </CloudPrintConfigSection>`

Please note that one can have multiple `Configuration` elements in the `CloudPrintConfigSection`.

Recommended configuration is to not download before print. But then only RawPrint option is available (printer must support it). If download is necessary use FoxIt over Adobe because FoxIt prints without firing a dialog (opening i dialog in windows service mode is not a good idea).
If downloading is used, then clean up is performed every 100th polling attempt, removing all pdf files in download folder older than 30 mins.

### NLog.config

NLog is used to produce trace, info and error messages from the application. This file is used to configure this (level of logging, logging destinations etc.). 
Two sections needs to be configured: `target` and `rules`. 

 `<target name="logfile" 
            xsi:type="File" 
            fileName="${basedir}/logs/log.txt" 
            archiveFileName="${basedir}/logs/archive.{#}.log"
            archiveEvery="Sunday"
            archiveNumbering="Rolling"
            maxArchiveFiles="30" />`

 `<logger name="*" minlevel="Trace" writeTo="logfile" />`

Please see NLog documention for more possibilities. https://nlog-project.org/

## Remarks
This application uses 
- NLog for logging https://nlog-project.org/
- RawPrint for raw printing https://www.nuget.org/packages/RawPrint/
- Adobe for local PDF printing https://acrobat.adobe.com/se/sv/acrobat/pdf-reader.html
- FoxIt for local PDF printing https://www.foxitsoftware.com/


