﻿using System;
using System.IO;
using System.Configuration;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks;

namespace CloudPrint
{
    class AzureStorageConnector : IDisposable
    {
        string storageConnection;
        private static CloudStorageAccount cloudStorageAccount;
        private static CloudBlobClient blobClient;
        private const string printTryCountKey = "PrintTryCount";

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public AzureStorageConnector()
        {
            storageConnection = ConfigurationManager.ConnectionStrings["BlobStorage"].ConnectionString;
            cloudStorageAccount = CloudStorageAccount.Parse(storageConnection);
            blobClient = cloudStorageAccount.CreateCloudBlobClient();
        }

        public void Dispose()
        {
            blobClient = null;
            cloudStorageAccount = null;
            storageConnection = null;
        }

        private CloudBlockBlob GetBlockBlob(string container, string fileName)
        {
            CloudBlobContainer cloudBlobContainer = blobClient.GetContainerReference(container);
            CloudBlockBlob blockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);

            return blockBlob;
        }

        public MemoryStream GetFileAsStream(string container, string fileName)
        {
            MemoryStream printStream = new MemoryStream();

            try
            {
                logger.Trace("Downloading {0} as MemoryStream.", fileName);

                CloudBlockBlob blockBlob = this.GetBlockBlob(container, fileName);
                blockBlob.DownloadToStream(printStream);                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error when trying to download file {0} as MemoryStream. Exception: {1}.", fileName, ex.Message);
            }

            return printStream;
        }

        public bool DownloadFile(string container, string fileName, string path)
        {
            try
            {
                logger.Trace("Downloading {0} to file {1}.", fileName, path);

                CloudBlockBlob blockBlob = this.GetBlockBlob(container, fileName);
                blockBlob.DownloadToFile(path, FileMode.Create);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error when trying to download file {0} as MemoryStream. Exception: {1}.", fileName, ex.Message);
                return false;
            }

            return true;
        }

        public List<string> GetFilesInDirectory(string directory, string fileTypeEnding)
        {
            CloudBlobContainer cloudBlobContainer = blobClient.GetContainerReference(directory);

            var blobs = cloudBlobContainer.ListBlobs(useFlatBlobListing: true);
            var files = new List<string>();
            
            foreach (var blob in blobs)
            {
                if (blob is CloudBlockBlob cbb)
                {
                    if (cbb.Name.IndexOf("/") == -1 //Only take files in 'top directory' (not sub directories)
                        && cbb.Name.ToUpper().EndsWith(fileTypeEnding.ToUpper())) //Only take .pdf files (or whatever ending is passed)
                    {
                        files.Add(blob.Uri.PathAndQuery);
                    }
                }
            }

            logger.Trace("Found {0} files in container {1}", files.Count, directory);

            return files;
        }

        public void MoveFileAsync(string sourceContainer, string destinationSubContainer, string fileName)
        {
            if (sourceContainer == "" 
                || destinationSubContainer == "" 
                || fileName == "")
            {
                logger.Error("MoveFileAsync: incorrectly called.");
            }

            var sourceBlob = this.GetBlockBlob(sourceContainer, fileName);
            var destinationBlob = this.GetBlockBlob(sourceContainer + "/" + destinationSubContainer, fileName);

            logger.Trace(string.Format("Copying the file {0} from {1} to {2}.", fileName, sourceBlob.Uri.PathAndQuery, destinationBlob.Uri.PathAndQuery));
            destinationBlob.StartCopy(sourceBlob); // If archive container doesnt exist, it will be created!

            logger.Trace(string.Format("Deleting the file {0} from {1}.", fileName, sourceBlob.Uri.PathAndQuery));
            sourceBlob.Delete(DeleteSnapshotsOption.IncludeSnapshots);

            logger.Info(string.Format("File {0} moved from {1} to {2}.", fileName, sourceContainer, destinationSubContainer));
        }

        public int GetCurrentPrintTryCount(string container, string fileName)
        {
            if (container == ""
                || fileName == "")
            {
                logger.Error("GetCurrentPrintTryCount: incorrectly called.");
            }

            int currentPrintCount = 0;

            try
            {
                var blob = this.GetBlockBlob(container, fileName);
                blob.FetchAttributes();

                if (!blob.Metadata.ContainsKey(printTryCountKey))
                {
                    return currentPrintCount;
                }
                else
                {
                    if (!int.TryParse(blob.Metadata[printTryCountKey], out currentPrintCount))
                    {
                        throw new InvalidCastException(string.Format("Blob meta data {0} must be able to cast to integer.", printTryCountKey));
                    }

                    return currentPrintCount;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Something went wrong when trying to get print out count on blob metadata.");
            }

            return 0; // Should not reach this point
        }

        public void IncreasePrintTryCount(string container, string fileName)
        {
            if (container == ""
                || fileName == "")
            {
                logger.Error("IncreasePrintTryCount: incorrectly called.");
            }

            try
            {
                var blob = this.GetBlockBlob(container, fileName);
                blob.FetchAttributes();

                foreach(var key in blob.Metadata.Keys)
                {
                    logger.Info(key.ToString());
                }

                if (!blob.Metadata.Keys.Contains(printTryCountKey))
                {
                    blob.Metadata.Add(printTryCountKey, 1.ToString());
                    blob.SetMetadata();

                    logger.Trace("Initilizing print retry count to {0} for file {1}.", 1.ToString(), fileName);
                }
                else
                {
                    string curCount = blob.Metadata[printTryCountKey];

                    if (int.TryParse(curCount, out int curCountInt))
                    {
                        curCountInt++;
                        blob.Metadata[printTryCountKey] = curCountInt.ToString();
                        blob.SetMetadata();

                        logger.Trace("Setting print retry count to {0} for file {1}.", curCountInt.ToString(), fileName);
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex, "Something went wrong when trying to set print out count on blob metadata.");
            }

            return;
        }
    }
}
