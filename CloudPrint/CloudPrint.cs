﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CloudPrint.Config;

namespace CloudPrint
{
    class CloudPrint
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        int eventId = 0;
        DocumentManager dm;
        List<CloudPrintConfig> configs;

        public void Run()
        {
            dm = new DocumentManager();
            configs = CloudPrintConfigManager.LoadConfigsFromFile();

            // Set up a timer 
            System.Timers.Timer timer = new System.Timers.Timer
            {
                Interval = 3 * 1000 // transform to milliseconds
            };
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();

            while (true)
            { }
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            if (eventId > 1000000)
            {
                eventId = 0;
            }

            logger.Trace("Scanning for new documents to print. Current execution count {0} (restored to 0 every 1 000 000 times).", eventId++);

            dm.PrintAllDocuments(configs);
        }
    }
}
