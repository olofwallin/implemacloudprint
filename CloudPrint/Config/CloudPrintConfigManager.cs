﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CloudPrint.Config
{
    public class CloudPrintConfigManager
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static List<CloudPrintConfig> LoadTemplateConfigs()
        {
            List<CloudPrintConfig> loadedConfigs = new List<CloudPrintConfig>
            {
                new CloudPrintConfig("test printer name", "shipmentdocs", "Archive", "Failed", true, "Adobe", "C:\\Temp\\")
            };

            return loadedConfigs;
        }

        public static List<CloudPrintConfig> LoadConfigsFromFile()
        {
            List<CloudPrintConfig> loadedConfigs = new List<CloudPrintConfig>();

            try
            {
                var cloudConfigurations = ConfigurationManager.GetSection("CloudPrintConfigSection") as CloudPrintConfigurationSection;

                foreach (CloudPrintConfig cloudConfig in cloudConfigurations.Configurations)
                {
                    cloudConfig.Validate();
                    loadedConfigs.Add(cloudConfig);
                    logger.Info("Loaded config: {0}", cloudConfig.ToString());
                }
            }
            catch
            {

            }
            return loadedConfigs;
        }

        public static void SaveConfigToFile(string folderName, string printername, string archiveFolder, string failedFolder)
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            var cloudConfigurations = configFile.Sections["CloudPrintConfigSection"] as CloudPrintConfigurationSection;
            cloudConfigurations.Configurations.Add(new CloudPrintConfig { FolderName = folderName, PrinterName = printername, ArchiveFolderName = archiveFolder, FailedFolderName = failedFolder });
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("CloudPrintConfigSection");

            logger.Info("Configuration saved to file.");
        }
    }
}
