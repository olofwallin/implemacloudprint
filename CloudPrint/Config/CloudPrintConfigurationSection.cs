﻿using System.Configuration;

namespace CloudPrint.Config
{
    class CloudPrintConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("Configurations", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(CloudPrintConfigurationCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove",
            CollectionType = ConfigurationElementCollectionType.BasicMap)]
        public CloudPrintConfigurationCollection Configurations
        {
            get
            {
                return (CloudPrintConfigurationCollection)base["Configurations"];
            }
            set
            {
                //value = (CloudPrintConfig)this["CloudPrintConfiguration"];
            }
        }
    }
}
