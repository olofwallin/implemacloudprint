﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace CloudPrint.Config
{
    class CloudPrintConfigurationCollection : ConfigurationElementCollection
    {

        public CloudPrintConfigurationCollection()
        {

        }

        public CloudPrintConfig this[int index]
        {
            get { return (CloudPrintConfig)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public void Add(CloudPrintConfig printConfig)
        {
            BaseAdd(printConfig);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new CloudPrintConfig();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var config = (CloudPrintConfig)element;
            return config.FolderName;
        }

        public void Remove(CloudPrintConfig printConfig)
        {
            BaseRemove(printConfig.FolderName);
        }
    }
}
