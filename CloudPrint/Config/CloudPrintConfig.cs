﻿using System.Configuration;

namespace CloudPrint.Config
{
    public class CloudPrintConfig : ConfigurationElement
    {
        [ConfigurationProperty("FolderName", IsKey = true, IsRequired = true)]
        public string FolderName
        { 
            get { return base["FolderName"] as string; }
            set { base["FolderName"] = value; }
        }

        [ConfigurationProperty("PrinterName", IsRequired = true)]
        public string PrinterName
        { 
            get { return base["PrinterName"] as string; }
            set { base["PrinterName"] = value; }
        }

        [ConfigurationProperty("ArchiveFolderName", IsRequired = true)]
        public string ArchiveFolderName
        {
            get { return base["ArchiveFolderName"] as string; }
            set { base["ArchiveFolderName"] = value; }
        }

        [ConfigurationProperty("FailedFolderName", IsRequired = true)]
        public string FailedFolderName
        {
            get { return base["FailedFolderName"] as string; }
            set { base["FailedFolderName"] = value; }
        }

        [ConfigurationProperty("DownloadBeforePrint", IsRequired = true)]
        public bool DownloadBeforePrint
        {
            get { return (bool)base["DownloadBeforePrint"]; }
            set { base["DownloadBeforePrint"] = value; }
        }

        [ConfigurationProperty("PrintWith", IsRequired = true)]
        public string PrintWith
        {
            get { return base["PrintWith"] as string; }
            set { base["PrintWith"] = value; }
        }

        [ConfigurationProperty("FileDownloadPath", IsRequired = false)]
        public string FileDownloadPath
        {
            get { return base["FileDownloadPath"] as string; }
            set { base["FileDownloadPath"] = value; }
        }

        public CloudPrintConfig() { }

        public CloudPrintConfig(string _printerName
                                , string _folderName
                                , string _archiveFolderName
                                , string _failedFolderName
                                , bool _downloadBeforePrint
                                , string _printWith
                                , string _fileDownloadPath)
        {
            PrinterName = _printerName;
            FolderName = _folderName;
            ArchiveFolderName = _archiveFolderName;
            FailedFolderName = _failedFolderName;
            DownloadBeforePrint = _downloadBeforePrint;
            PrintWith = _printWith;
            FileDownloadPath = _fileDownloadPath;
        }

        /// <summary>
        /// This function validates the CloudPrintConfig and throws
        /// exceptions if not valid.
        /// </summary>
        public void Validate()
        {
            if (this.DownloadBeforePrint 
                && this.FileDownloadPath == string.Empty)
            {
                throw new System.ArgumentException("If DownloadBeforePrint is set, a FileDownloadPath must be provided in the configuration");
            }
        }

        public override string ToString()
        {
            return string.Format("Incoming folder: {0}, Archive folder: {1}, Failed folder: {2}, Printer name: {3}, DownloadBeforePrint: {4}, PrintWith: {5}, FileDownloadPath: {6}"
                                , this.FolderName
                                , this.ArchiveFolderName
                                , this.FailedFolderName
                                , this.PrinterName
                                , this.DownloadBeforePrint
                                , this.PrintWith
                                , this.FileDownloadPath);
        }
    }
}
