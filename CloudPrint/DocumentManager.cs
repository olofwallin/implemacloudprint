﻿using System;
using System.IO;
using System.Collections.Generic;
using CloudPrint.Config;
using RawPrint;
using System.Diagnostics;

namespace CloudPrint
{
    public class DocumentManager
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public void PrintAllDocuments(List<CloudPrintConfig> configs)
        {
            try
            {
                foreach (var config in configs)
                {
                    logger.Trace("Trying to find files in container {0}.", config.FolderName);
                    using (AzureStorageConnector storageConnector = new AzureStorageConnector())
                    {
                        foreach (var fileName in storageConnector.GetFilesInDirectory(config.FolderName, ".pdf"))
                        {
                            string container = fileName.Substring(1, fileName.IndexOf("/", 1) - 1);
                            string filenameSpec = fileName.Substring(fileName.LastIndexOf("/") + 1);

                            if (storageConnector.GetCurrentPrintTryCount(container, filenameSpec) >= 5) // Max print try count reached
                            {
                                storageConnector.MoveFileAsync(container, config.FailedFolderName, filenameSpec);
                            }

                            bool printSuccessful = false;

                            if (config.DownloadBeforePrint)
                            {
                                
                                string filePath = Path.Combine(config.FileDownloadPath, filenameSpec);

                                if (storageConnector.DownloadFile(container, filenameSpec, filePath))
                                {
                                    switch (config.PrintWith)
                                    {
                                        case "Adobe":
                                            printSuccessful = PrintDocumentFromFileWithAdobe(filePath, config.PrinterName, filenameSpec);
                                            break;
                                        case "FoxIt":
                                            printSuccessful = PrintDocumentFromFileWithFoxIt(filePath, config.PrinterName, filenameSpec);
                                            break;
                                    }

                                    // Clean up is done by clean up routine initiated bo service
                                }
                            }
                            else
                            {
                                MemoryStream msFile = storageConnector.GetFileAsStream(container, filenameSpec);

                                if (msFile != null)
                                {
                                    printSuccessful = PrintDocument(msFile, config.PrinterName, filenameSpec);
                                }
                            }

                            if (printSuccessful)
                            {
                                storageConnector.MoveFileAsync(container, config.ArchiveFolderName, filenameSpec);
                            }
                            else
                            {
                                storageConnector.IncreasePrintTryCount(container, filenameSpec);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error caught when trying to print documents");
            }
        }

        public void RemoveDownloadedFiles(List<CloudPrintConfig> configs)
        {
            DateTime expireDateTime = DateTime.UtcNow.AddMinutes(-30);

            foreach (var config in configs)
            {
                if (config.DownloadBeforePrint && config.FileDownloadPath != string.Empty)
                {
                    logger.Trace("Removing files from {0} older than {1}", config.FileDownloadPath, expireDateTime.ToString());
                    foreach (var filepath in Directory.EnumerateFiles(config.FileDownloadPath, "*.pdf", SearchOption.TopDirectoryOnly))
                    {
                        if (File.GetCreationTimeUtc(filepath) < expireDateTime)
                        {
                            this.RemoveFile(filepath);
                        }
                    }
                }
            }
        }

        private bool RemoveFile(string path)
        {
            if (File.Exists(path))
            {
                try
                {
                    logger.Trace("Removing file {0}", path);
                    File.Delete(path);
                }
                catch (IOException ioExp)
                {
                    logger.Error(ioExp, "Error deleting file {0}", path);
                    return false;
                }
            }

            return true;
        }

        private bool PrintDocumentFromFileWithFoxIt(string filePath, string printerName, string fileName)
        {
            
            try
            {

                string pdfPrinterLocation = @"""C:\Program Files (x86)\Foxit Software\Foxit Reader\FoxitReader.exe""";
                string pdfArguments = string.Format(" /t " + "\"" + filePath + "\" " + "\""+ printerName + "\"");

                logger.Trace("Printing document {0} at {1} on printer {2} with FoxIt. Printer executable {3}, printer arguments {4}", fileName, filePath, printerName, pdfPrinterLocation, pdfArguments);

                ProcessStartInfo newProcess = new ProcessStartInfo(pdfPrinterLocation, pdfArguments);
                newProcess.CreateNoWindow = true;
                newProcess.RedirectStandardOutput = true;
                newProcess.UseShellExecute = false;

                Process pdfProcess = new Process();
                pdfProcess.StartInfo = newProcess;
                pdfProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                pdfProcess.Start();
                pdfProcess.WaitForExit(); 

                int counter = 0;
                while (!pdfProcess.HasExited)
                {
                    System.Threading.Thread.Sleep(1000);
                    counter += 1;
                    if (counter == 5) break;
                }
                if (!pdfProcess.HasExited)
                {
                    pdfProcess.CloseMainWindow();
                    pdfProcess.Kill();
                }

                logger.Info("Printed document {0} on printer {1} with FoxIt", fileName, printerName);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Document {0} could not be printed on {1} with FoxIt. Error message: {2}. Inner exception: {3}. Stack trace: {4}", fileName, printerName, ex.Message, ex.InnerException, ex.StackTrace);
                return false;
            }

            return true;


        }

        private bool PrintDocumentFromFileWithAdobe(string filePath, string printerName, string fileName)
        {
            try
            {
                string processFilename = Microsoft.Win32.Registry.LocalMachine
                        .OpenSubKey("Software")
                        .OpenSubKey("Microsoft")
                        .OpenSubKey("Windows")
                        .OpenSubKey("CurrentVersion")
                        .OpenSubKey("App Paths")
                        .OpenSubKey("AcroRd32.exe")
                        .GetValue(String.Empty).ToString();

                ProcessStartInfo info = new ProcessStartInfo();
                //info.Verb = "print";
                info.FileName = "\"" + processFilename + "\"";
                info.Arguments = String.Format("/s /h /t \"{0}\" \"{1}\"", filePath, printerName);
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;
                //(It won't be hidden anyway... thanks Adobe!)
                info.UseShellExecute = true;

                logger.Trace("Printing document {0} at {1} on printer {2} with Adobe. Print application {3} , print arguments {4}.", fileName, filePath, printerName, info.FileName, info.Arguments);

                Process p = Process.Start(info);
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                int counter = 0;
                while (!p.HasExited)
                {
                    System.Threading.Thread.Sleep(1000);
                    counter += 1;
                    if (counter == 5) break;
                }
                if (!p.HasExited)
                {
                    p.CloseMainWindow();
                    p.Kill();
                }

                logger.Info("Printed document {0} on printer {1} with Adobe", fileName, printerName);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Document {0} could not be printed on {1} with Adobe. Error message: {2}. Inner exception: {3}. Stack trace: {4}", fileName, printerName, ex.Message, ex.InnerException, ex.StackTrace);
                return false;
            }

            return true;
        }

        private bool PrintDocumentWithDotNet(MemoryStream fileStream, string printerName, string fileName)
        {
            logger.Trace("Printing document {0} on printer {1}", fileName, printerName);

            try
            {
                PrintDocumentDotNet dotNetPrinter = new PrintDocumentDotNet();
                dotNetPrinter.Print(fileStream, printerName);
                
                logger.Info("Printed document {0} on printer {1} with dot net", fileName, printerName);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Document {0} could not be printed on {1} with dot net. Error message: {2}. Inner exception: {3}. Stack trace: {4}", fileName, printerName, ex.Message, ex.InnerException, ex.StackTrace);
                return false;
            }

            return true;
        }

        private bool PrintDocument(MemoryStream fileStream, string printerName, string fileName)
        {
            logger.Trace("Printing document {0} on printer {1}", fileName, printerName);

            try
            {
                
                Printer printer = new Printer();
                printer.PrintRawStream(printerName, fileStream, fileName, false);

                logger.Info("Printed document {0} on printer {1}", fileName, printerName);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Document {0} could not be printed on {1}. Error message: {2}. Inner exception: {3}. Stack trace: {4}", fileName, printerName, ex.Message, ex.InnerException, ex.StackTrace);
                return false;
            }

            return true;
        }
    }
}
