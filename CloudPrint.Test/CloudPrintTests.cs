﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CloudPrint;
using CloudPrint.Config;
using System.Collections.Generic;

namespace CloudPrint.Test
{
    [TestClass]
    public class CloudPrintTests
    {
        [TestMethod]
        public void CloudPrint_Config_ShouldLoadTemplateConfigs()
        {
             List<CloudPrintConfig> configs = CloudPrintConfigManager.LoadTemplateConfigs();

                Assert.IsTrue(configs.Count > 0, "CloudPrintConfigManager.LoadTemplateConfigs() should have at least on default config");
        }

        [TestMethod]
        public void CloudPrint_Config_ShouldHaveDownloadPathSet()
        {
            CloudPrintConfig config = new CloudPrintConfig("printerName", "folderName", "archiveFolderName", "failedFolderName", true, "printWith", "");
            Assert.ThrowsException<ArgumentException>(() => config.Validate());
        }
    }
}
